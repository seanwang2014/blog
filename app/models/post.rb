class Post < ActiveRecord::Base
	has_many :comments, dependent: :destory
	validates_presence_of :title
	validates_presence_of :body
end
